import React from 'react';
import { HashRouter, Route, Switch } from 'react-router-dom';
import { Col, Container, Row } from 'react-bootstrap';
import { relative } from 'path';
import Rory from './public/rory.png';

const Home = React.lazy(() => import(/* webpackChunkName: "Home" */ './pages/Home'));
const Projects = React.lazy(() => import(/* webpackChunkName: "Projects" */ './pages/Projects'));
const Family = React.lazy(() => import(/* webpackChunkName: "Family" */ './pages/Family'));
const NotFound = React.lazy(() => import(/* webpackChunkName: "NotFound" */ './pages/NotFound'));

export default function App(): JSX.Element {
  return (
    <Container>
      <Row>
        <Col className="mx-auto" md={{ span: 8 /* offset: 2 */ }}>
          <Container className="main shadow p-2 rounded">
            <HashRouter>
              <React.Suspense
                fallback={(
                  <div className="d-flex justify-content-center">
                    <div className="spinner-border" role="status" />
                  </div>
                )}
              >
                <Switch>
                  <Route exact path="/" component={Home} />
                  <Route path="/projects" component={Projects} />
                  <Route path="/family" component={Family} />
                  <Route component={NotFound} />
                </Switch>
              </React.Suspense>
            </HashRouter>
          </Container>
        </Col>
      </Row>
      <div
        id="mascot"
        className="container-fluid fixed-bottom zindex-n1 d-flex justify-content-center justify-content-xl-end"
      >
        <Row>
          <img src={Rory} className="mx-auto" />
        </Row>
      </div>
    </Container>
  );
}
