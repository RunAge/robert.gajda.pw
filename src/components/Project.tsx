import React, { Component } from 'react';
import { Card, Button } from 'react-bootstrap';

export interface ProjectProps {
  title: string;
  description: string;
  maintained: boolean;
  repo: string;
}

export default class Project extends Component {
  private constructor(props: ProjectProps) {
    super(props);
    this.state = props.project;
  }

  public render(): JSX.Element {
    const {
      title, description, maintained, repo,
    } = this.state;
    return (
      <Card bg="dark" className="shadow p-2 rounded">
        <a href={repo} className="text-decoration-none">
          <Card.Title>{title}</Card.Title>
        </a>
        <Card.Text>{description}</Card.Text>
        <Card.Text>
          Maintained:
          {maintained ? ' Yes' : ' No'}
        </Card.Text>
      </Card>
    );
  }
}
