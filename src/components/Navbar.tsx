import React from 'react';
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Navbar(): JSX.Element {
  return (
    <div className="d-flex justify-content-center pb-0 pt-2">
      <Link to="/">
        <Button variant="link" className="color-removed text-decoration-none">
          Home
        </Button>
      </Link>

      <Link to="/projects">
        <Button variant="link" className="color-removed text-decoration-none">
          Projects
        </Button>
      </Link>

      {/* <Link to="/other">
        <Button variant="link" className="color-removed text-decoration-none">
          Other
        </Button>
      </Link> */}
      <Link to="/family">
        <Button variant="link" className="color-removed text-decoration-none">
          Family
        </Button>
      </Link>
    </div>
  );
}
