import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faGitlab, faLinkedin, faFacebook } from '@fortawesome/free-brands-svg-icons';
import { Row, Col, Button } from 'react-bootstrap';
import Navbar from '../components/Navbar';

export default function Home(): JSX.Element {
  return (
    <div>
      <Row className="bor-bottom ">
        <Col
          lg
          md={{ span: 12 }}
          className="d-flex justify-content-lg-start justify-content-center"
        >
          <h4 className="align-self-center p-2 pb-2">
            Robert
            <span className="color-removed"> &apos;RunAge&apos; </span>
            Gajda
          </h4>
        </Col>
        <Col lg md={{ span: 12 }} className="d-flex justify-content-lg-end justify-content-center">
          <a href="https://gitlab.com/RunAge">
            <Button variant="link" size="lg" className="color-removed">
              <FontAwesomeIcon icon={faGitlab} />
            </Button>
          </a>
          <a href="https://www.linkedin.com/in/runage/">
            <Button variant="link" size="lg" className="color-removed">
              <FontAwesomeIcon icon={faLinkedin} />
            </Button>
          </a>
          <a href="https://www.facebook.com/yuki.runage">
            <Button variant="link" size="lg" className="color-removed">
              <FontAwesomeIcon icon={faFacebook} />
            </Button>
          </a>
        </Col>
      </Row>
      <Row>
        <Col>
          <Navbar />
        </Col>
      </Row>
    </div>
  );
}
