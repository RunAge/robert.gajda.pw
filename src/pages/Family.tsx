import React from 'react';
import {
  Container, Row, Col, Card,
} from 'react-bootstrap';
import Navbar from '../components/Navbar';

export default function Home(): JSX.Element {
  return (
    <div>
      <Row className="bor-bottom ">
        <Col
          lg
          md={{ span: 12 }}
          className="d-flex justify-content-lg-start justify-content-center"
        >
          <h4 className="align-self-center p-2 pb-2">Family</h4>
        </Col>
      </Row>
      <Row>
        <Col>
          <Navbar />
        </Col>
      </Row>
      <Row>
        <Col>
          <Container className="pb-4">
            <Row>
              <Col className="pt-2">
                <Card bg="dark" className="shadow p-2 rounded">
                  <Card.Title>
                    xxx
                    {' '}
                    <small className="text-muted">Elektryk</small>
                  </Card.Title>
                  <Card.Text>
                    Kontakt:
                    {' '}
                    <a href="https://xxx.gajda.pw">xxx.gajda.pw</a>
                  </Card.Text>
                </Card>
              </Col>
            </Row>
            <Row>
              <Col className="pt-2">
                <Card bg="dark" className="shadow p-2 rounded">
                  <Card.Title>
                    xxx
                    {' '}
                    <small className="text-muted">F.H.U xxx Gajda</small>
                  </Card.Title>
                  <Card.Text>
                    Kontakt:
                    {' '}
                    <a href="https://xxx.gajda.pw">xxx.gajda.pw</a>
                  </Card.Text>
                </Card>
              </Col>
            </Row>
          </Container>
        </Col>
      </Row>
    </div>
  );
}
