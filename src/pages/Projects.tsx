import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faGitlab, faLinkedin, faFacebook } from '@fortawesome/free-brands-svg-icons';
import { Container, Row, Col } from 'react-bootstrap';
import Navbar from '../components/Navbar';
import Project from '../components/Project';
// import * as projects from '../data/projects.json';

export default class Projects extends Component {
  private constructor(props) {
    super(props);
    this.state = {
      projects: [],
      isLoading: false,
    };
  }

  public componentDidMount(): void {
    this.setState({ isLoading: true });
    fetch('https://files.catbox.moe/ye5ofd.json')
      .then((data) => data.json())
      .then((json) => this.setState({
        projects: json,
        isLoading: false,
      }))
      .catch((err) => {
        // SAME ORIGIN POLICY WORKAROUND
        this.setState({
          projects: [
            {
              title: 'robert.gajda.pw',
              description: 'This website.',
              maintained: true,
              repo: 'https://gitlab.com/RunAge/robert.gajda.pw',
            },
            {
              title: 'Rory',
              description: 'Modular Discord bot.',
              maintained: false,
              repo: 'https://gitlab.com/RunAge/Rory',
            },
            {
              title: 'SSSS',
              description: 'Simple ScreenShot Server.',
              maintained: false,
              repo: 'https://gitlab.com/RunAge/SSSS',
            },
            {
              title: 'NowPlaying-foobar2000-',
              description: 'Small script to output now played song in foobar2000 to file.',
              maintained: false,
              repo: 'https://gitlab.com/RunAge/NowPlaying-foobar2000-',
            },
            {
              title: 'auth.kamide.re',
              description: 'OAuth2 Server focused on privacy and data safety.',
              maintained: false,
              repo: 'https://gitlab.com/RunAge/auth.kamide.re',
            },
          ],
          isLoading: false,
        });
        console.log(err);
      });
  }

  public render(): JSX.Element {
    const { projects, isLoading } = this.state;
    return (
      <div>
        <Row className="bor-bottom ">
          <Col
            lg
            md={{ span: 12 }}
            className="d-flex justify-content-lg-start justify-content-center"
          >
            <h4 className="align-self-center p-2 pb-2">Projects</h4>
          </Col>
        </Row>
        <Row>
          <Col>
            <Navbar />
          </Col>
        </Row>
        <Row>
          <Col>
            <Container className="pb-4">
              {(() => {
                if (isLoading) {
                  return (
                    <div className="d-flex justify-content-center">
                      <div className="spinner-border" role="status">
                        <span className="sr-only">Loading...</span>
                      </div>
                    </div>
                  );
                }
              })()}
              {projects.map((project) => (
                <Row key={project.title}>
                  <Col className="pt-2">
                    <Project project={project} />
                  </Col>
                </Row>
              ))}
            </Container>
          </Col>
        </Row>
      </div>
    );
  }
}
