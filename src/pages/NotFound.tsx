import React from 'react';
import { Row, Col, Button } from 'react-bootstrap';
import Navbar from '../components/Navbar';

export default function NotFound() {
  return (
    <div>
      <Row className="bor-bottom ">
        <Col
          lg
          md={{ span: 12 }}
          className="d-flex justify-content-lg-start justify-content-center"
        >
          <h4 className="align-self-center p-2 pb-2">
            <span className="color-removed"> 404 </span>
            {' '}
Not Found
          </h4>
        </Col>
      </Row>
      <Row>
        <Col>
          <Navbar />
        </Col>
      </Row>
    </div>
  );
}
